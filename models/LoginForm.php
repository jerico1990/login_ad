<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            //['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword']
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user) {
                $this->addError($attribute, 'Incorrecto usuario o password');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            //var_dump($this->getUser());die;
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        //var_dump($this->getLdap());die;
        if ($this->_user === false && $this->getLdap()) {
            $this->_user = User::findByUsername($this->username,$this->password);
            //var_dump($this->_user);die;
        }

        return $this->_user;
    }


    public function getLdap(){
        // Credenciales de prueba
        $user = "mcruz@pais.gob.pe";
        $pass = "Sistemas2008";

        // Datos de acceso al servidor LDAP
        $host = "INTI.PAIS.GOB.PE";
        $port = "389";

        // Conexto donde se encuentran los usuarios
        $basedn = "DC=pais,DC=gob,DC=pe";
        //CN=Usuarios del dominio,CN=Users,
        // Atributos a recuperar
        $filter = "(&(objectClass=*)(objectCategory=*)(samaccountname=ldap.server))";
        //(objectCategory=person)
        // Atributo para incorporar en la respuesta
        //$attributes = array("displayName","description","cn","givenName","sn","mail","co","telephonenumber","company","displayName","memberof");
        $attributes = array("cn","sn","title","description","physicaldeliveryofficename","telephonenumber","facsimiletelephonenumber","givenname","distinguishedname","displayname","department","name","userprincipalname");
        // Establecer la conexión con el servidor LDAP
        $ad = ldap_connect($host,$port) or die("No se pudo conectar al servidor LDAP.");
        //var_dump(ldap_bind($ad));die;
        // Autenticar contra el servidor LDAP
        ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ad, LDAP_OPT_REFERRALS,0);

        //var_dump(ldap_bind($ad, $user, $pass));
        //var_dump("aaa");die;
        if (@ldap_bind($ad, $user, $pass)) {
            //var_dump("aaa");die;
            // En caso de éxito, recuperar los datos del usuario
            $result = ldap_search($ad, $basedn, $filter,$attributes,0,1,0);

            
            $entries = ldap_get_entries($ad, $result);
            var_dump($this->entriesToArray($entries));die;
            

        }
        else {
                // Si falla la autenticación, retornar error
                $msg = "Usuario y/o contraseña inválidos";
        }
        ldap_close($ad);

        die;
       
    }

    public function entriesToArray($entries=array())
    {
        $list  = array();
        for($i=0;$i<$entries['count'];$i++)
            {
            $row = array();
            $dn  = $entries[$i]['dn'];
            unset($entries[$i]['count']);
            unset($entries[$i]['dn']);
            foreach($entries[$i] as $key=>$value)
                {
                if (is_int($key))
                    {continue;}
                $list_in_col = array();
                foreach($value as $key2=>$value2)
                    {
                    if (!is_int($key2))
                        {continue;}
                    $list_in_col[] = $value2;
                    }
                $row[$key] = $list_in_col;
                }
            $row['dn'] = $dn;
            $list[$i] = $row;
            }
        return $list;
    }

    // public function getLdap(){
    //     // Credenciales de prueba
    //     $user = "nobel";
    //     $pass = "password";

    //     // Datos de acceso al servidor LDAP
    //     $host = "ldap.forumsys.com";
    //     $port = "389";

    //     // Conexto donde se encuentran los usuarios
    //     $basedn = "cn=read-only-admin,dc=example,dc=com";

    //         // Atributos a recuperar
    //     $searchAttr = array("ou","dn", "ouid", "sn", "givenName");
    //         // Atributo para incorporar en la respuesta
    //     $displayAttr = "cn";

    //     // Respuesta por defecto
    //     $status = 1;
    //     $msg = "";
    //     $userDisplayName = "null";

    //     // Establecer la conexión con el servidor LDAP
    //     $ad = ldap_connect("ldap://{$host}",$port) or die("No se pudo conectar al servidor LDAP.");
    //     //var_dump($ad);die;
    //     // Autenticar contra el servidor LDAP

    //     ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
        
    //     if (@ldap_bind($ad, $basedn, $pass)) {
    //             var_dump("ingreso");
    //             // En caso de éxito, recuperar los datos del usuario
    //             $result = ldap_search($ad, $basedn, "(givenname={$user}*)", $searchAttr);
                
    //             $entries = ldap_get_entries($ad, $result);
    //             print_r($entries);die;
    //             if ($entries["count"]>0) {
    //                     // Si hay resultados en la búsqueda
    //                     $status = 0;
                        

    //                     if (isset($entries[0][$displayAttr])) {
    //                             // Recuperar el atributo a incorporar en la respuesta
    //                             $userDisplayName = $entries[0][$displayAttr][0];
                                
    //                             $msg = "Autenticado como {$userDisplayName}";
    //                     }
    //                     else {
    //                             // Si el atributo no está definido para el usuario
    //                             $userDisplayName = "-";
    //                             $msg = "Atributo no disponible ({$displayAttr})";
    //                     }
    //             }
    //             else {
    //                     // Si no hay resultados en la búsqueda, retornar error
    //                     $msg = "Error desconocido";
    //             }
    //     }
    //     else {
    //             // Si falla la autenticación, retornar error
    //             $msg = "Usuario y/o contraseña inválidos";
    //     }
    //     die;
        
    //     // Respuesta en formato JSON
    //     header('Content-Type: application/json');
    //     echo "{\"uid\": \"{$user}\", \"estado\": \"{$status}\", \"nombre\": \"{$userDisplayName}\", \"debug\": \"{$msg}\"}";
    // }

    // public function getLdap(){
    //     #192.168.10.20
    //     #
    //     $ldap=ldap_connect('ldap://INTI.PAIS.GOB.PE',389);
    //     if($ldap){
    //         ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
    //         ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
    //         $authenticated = @ldap_bind($ldap,$this->username,$this->password);
            
    //         if ($authenticated) {
    //             return true;
    //         }
    //         return false;
    //     }        
    //     return false;
    // }
}
